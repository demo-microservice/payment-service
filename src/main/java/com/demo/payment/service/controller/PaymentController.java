package com.demo.payment.service.controller;

import com.demo.payment.service.entity.Payment;
import com.demo.payment.service.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.Random;

@RestController
@RequestMapping("/api")
public class PaymentController {

    @Autowired
    private PaymentService service;

    @PostMapping("/payment")
    public Payment doPayment(@RequestBody Payment payment) {
        return service.doPayment(payment);
    }

    @GetMapping("/payment")
    public List<Payment> getAllPayment() {
        return service.getAllPayment();
    }

    @GetMapping("/payment/{paymentId}")
    public Optional<Payment> getPaymentById(@PathVariable("paymentId") String paymentId) {
        return service.getPaymentById(paymentId);
    }

    @GetMapping("/payment/order/{orderId}")
    public Payment getPaymentByOrderId(@PathVariable("orderId") String orderId) {
        return service.getPaymentByOrderId(orderId);
    }

    @DeleteMapping("/payment/delete")
    public String deletePaymentById() {
        return service.deletePaymentById();
    }
}
