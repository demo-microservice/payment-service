package com.demo.payment.service.service;

import com.demo.payment.service.entity.Payment;
import com.demo.payment.service.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

@Service
public class PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    public Payment doPayment(Payment payment) {
        payment.setPaymentStatus(paymentProcessing());
        payment.setTransactionId(UUID.randomUUID().toString());
        return paymentRepository.save(payment);
    }

    public List<Payment> getAllPayment() {
        return (List<Payment>) paymentRepository.findAll();
    }

    public Optional<Payment> getPaymentById(String paymentId) {
        return paymentRepository.findById(paymentId);
    }

    public Payment getPaymentByOrderId(String orderId) {
        return paymentRepository.findByOrderId(orderId);
    }

    public String deletePaymentById() {
        paymentRepository.deleteAll();
        return "successfully delete payment";
    }

    public String paymentProcessing() {
        return new Random().nextBoolean() ? "success" : "failed";
    }

}
